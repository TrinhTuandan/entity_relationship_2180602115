﻿namespace entity_relationship_2180602115.Models
{
    public class Employees
    {
        public int ID { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Contactandaddress { get; set; }
        public string? Usenameandpassword { get; set; }
    }
}
