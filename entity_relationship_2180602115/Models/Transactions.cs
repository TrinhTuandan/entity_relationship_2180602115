﻿namespace entity_relationship_2180602115.Models
{
    public class Transactions
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public string? Name { get; set; }
    }
}
