﻿namespace entity_relationship_2180602115.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Contactandaddress { get; set; }
        public string? Usename { get; set; }
        public string? Password { get; set; }
    }
}
