﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace entity_relationship_2180602115.Models;

public partial class BanhangContext : DbContext
{
    public BanhangContext()
    {
    }

    public BanhangContext(DbContextOptions<BanhangContext> options)
        : base(options)
    {
    }
    public DbSet<Accounts> accountsS { get; set; }
    public DbSet<Customer> customerS { get; set; }
    public DbSet<Employees> employeesS { get; set; }
    public DbSet<Logs> logsS { get; set; }
    public DbSet<Reports> reportsS { get; set; }
    public DbSet<Transactions> transactionsS { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=DESKTOP-9K1PP5I;Database=banhang;Trusted_Connection=True;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
