﻿namespace entity_relationship_2180602115.Models
{
    public class Logs
    {
        public int ID { get; set; }
        public int TransactionalID { get; set; }
        public DateTime? Logindate { get; set; }
        public DateTime? Logintime { get; set; }

    }
}
