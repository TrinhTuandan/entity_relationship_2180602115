﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace entity_relationship_2180602115.Migrations
{
    /// <inheritdoc />
    public partial class InitialModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "accountsS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerID = table.Column<int>(type: "int", nullable: false),
                    AccountsName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Addtexthare = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accountsS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "customerS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Contactandaddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Usename = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customerS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "employeesS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Contactandaddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Usenameandpassword = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employeesS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "logsS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionalID = table.Column<int>(type: "int", nullable: false),
                    Logindate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Logintime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logsS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "reportsS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountID = table.Column<int>(type: "int", nullable: false),
                    LogsID = table.Column<int>(type: "int", nullable: false),
                    TransactionalID = table.Column<int>(type: "int", nullable: false),
                    Reportname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Reportdate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reportsS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "transactionsS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeID = table.Column<int>(type: "int", nullable: false),
                    CustomerID = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transactionsS", x => x.ID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "accountsS");

            migrationBuilder.DropTable(
                name: "customerS");

            migrationBuilder.DropTable(
                name: "employeesS");

            migrationBuilder.DropTable(
                name: "logsS");

            migrationBuilder.DropTable(
                name: "reportsS");

            migrationBuilder.DropTable(
                name: "transactionsS");
        }
    }
}
